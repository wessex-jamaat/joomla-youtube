<?php defined('_JEXEC') or die;

try
{
	// Require the module helper file
	require_once __DIR__ . '/helper.php';

	// Get a new Helper instance
	$helper = new ModWJYouTubeHelper($params);

	// Setup joomla cache
	$cache = JFactory::getCache();
	$cache->setCaching(true);
	$cache->setLifeTime($params->get('api_cache_time', 60));

	// Get the next videos
//	$videos = $cache->call(
//		array($helper, 'getVideos'),
//		(int) $params->get('max_list_videos', 5)
//	);

    $videos = $helper->getVideos((int) $params->get('max_list_videos', 5));

	// Get the Layout
	require JModuleHelper::getLayoutPath('mod_wjyoutube', $params->get('layout', 'default'));
}
catch(Exception $e)
{
	JFactory::getApplication()->enqueueMessage(
		'WJ YouTube error: ' . $e->getMessage(), 'error'
	);
}
