<?php defined('_JEXEC') or die;

use Joomla\Registry\Registry;

/**
 * Class ModWJYouTubeHelper
 */
class ModWJYouTubeHelper {

	/**
	 * The google developer api key
	 *
	 * @var string
	 */
	protected $apiKey;

	/**
	 * The youtube channel id
	 *
	 * @var string
	 */
	protected $channelId;

	/**
	 * Create a new ModWJYouTubeHelper instance
	 *
	 * @param Registry $params
	 */
	public function __construct(Registry $params)
	{
		$this->apiKey    = $params->get('api_key', null);
		$this->channelId = $params->get('channel_id', null);
	}

	/**
	 * Get the next google events
	 *
	 * @param $maxEvents
	 *
	 * @return array
	 */
	public function getVideos($maxResults)
	{
		$options = array(
			'maxResults' => $maxResults
		);

		$videos = $this->fetchData($options);

		return $this->prepareVideos($videos);
	}

	/**
	 * Get the videos data from the google youtube api
	 *
	 * @param array $options The query parameter options
	 *
	 * @return mixed
	 *
	 * @throws UnexpectedValueException
	 */
	protected function fetchData($options)
	{
		$defaultOptions = array();

		$options = array_merge($defaultOptions, $options);

		// Create an instance of a default Http object.
		$http = JHttpFactory::getHttp();
		$url  = 'https://www.googleapis.com/youtube/v3/search?channelId='
			. urlencode($this->channelId) . '&key=' . urlencode($this->apiKey)
			. '&part=snippet&order=date'
			. '&' . http_build_query($options);

		$response = $http->get($url);
		$data     = json_decode($response->body);

		if ($data && isset($data->items))
		{
			return $data->items;
		}
		elseif ($data)
		{
			return array();
		}

		throw new UnexpectedValueException("Unexpected data received from Google: `{$response->body}`.");
	}

	/**
	 * Prepare videos
	 *
	 * @param $videos
	 *
	 * @return array
	 */
	protected function prepareVideos($videos)
	{
		foreach ($videos AS $i => $video)
		{
			$videos[$i] = $this->prepareVideo($video);
		}

		return $videos;
	}

	/**
	 * Prepare an event
	 *
	 * @param $event
	 *
	 * @return object
	 */
	protected function prepareVideo($event)
	{
	//	$event->startDate = $this->unifyDate($event->start);
	//	$event->endDate   = $this->unifyDate($event->end);
	//	$event->eventLink = $this->createEventLink($event);

		return $event;
	}

	/**
	 * Unify the api dates
	 *
	 * @param $date
	 *
	 * @return JDate
	 */
	protected function unifyDate($date)
	{
		$timeZone = (isset($date->timezone)) ? $date->timezone : null;

		if (isset($date->dateTime))
		{
			return JDate::getInstance($date->dateTime, $timeZone);
		}

		return JDate::getInstance($date->date, $timeZone);
	}

//	protected function createEventLink($event)
//	{
//        if (file_exists(JPATH_ADMINISTRATOR . '/components/com_googlecalendar/index.html') && JComponentHelper::isEnabled('com_googlecalendar', true))
//        {
//            return JRoute::_('index.php?option=com_googlecalendar') . '?event_id=' . urlencode($event->id);
//        }
//	    return $event->htmlLink;
//	}
}