<?php defined('_JEXEC') or die; ?>

<style>
div.mod-wjyoutube ul li { list-style: none; padding-left: 0px; }
div.mod-wjyoutube ul li img { width: 50px; }
div#mod-wjyoutube-player { z-index: 1000; position: fixed; left: 25%; top: 25%; width: 80%; height: 50%; }

</style>

<div class="mod-wjyoutube">
    <?php if (count($videos) > 0) { ?>
        <div id="mod-wjyoutube-player">
            <iframe width="100%" src="https://www.youtube.com/embed/0ZfS3tlp-5E?autoplay=1" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
        <ul>
            <?php foreach ($videos AS $video) : ?>
                <li>
                    <img src="<?php echo htmlspecialchars($video->snippet->thumbnails->default->url) ?>" />
                    <?php echo htmlspecialchars($video->snippet->title) ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php } else { ?>
        <div class="no-events">
            <?php echo htmlspecialchars($params->get('no_content_text')) ?>
        </div>
    <?php } //endif ?>
1.0.2
    <?php if (isset($moreLink)) { ?>
        <div class="module-more">
            <a href="<?php echo $moreLink ?>">More</a>
        </div>
    <?php } ?>
</div>